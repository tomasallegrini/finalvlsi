library verilog;
use verilog.vl_types.all;
entity vga_vlg_check_tst is
    port(
        azul_out        : in     vl_logic_vector(9 downto 0);
        rojo_out        : in     vl_logic_vector(9 downto 0);
        verde_out       : in     vl_logic_vector(9 downto 0);
        vga_blank       : in     vl_logic;
        vga_clk         : in     vl_logic;
        vga_hs          : in     vl_logic;
        vga_sync        : in     vl_logic;
        vga_vs          : in     vl_logic;
        sampler_rx      : in     vl_logic
    );
end vga_vlg_check_tst;
