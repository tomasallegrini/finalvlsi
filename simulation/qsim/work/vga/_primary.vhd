library verilog;
use verilog.vl_types.all;
entity vga is
    port(
        reset           : in     vl_logic;
        clock           : in     vl_logic;
        rojo_in         : in     vl_logic_vector(5 downto 0);
        verde_in        : in     vl_logic_vector(5 downto 0);
        azul_in         : in     vl_logic_vector(5 downto 0);
        vga_clk         : out    vl_logic;
        vga_sync        : out    vl_logic;
        vga_blank       : out    vl_logic;
        vga_vs          : out    vl_logic;
        vga_hs          : out    vl_logic;
        rojo_out        : out    vl_logic_vector(9 downto 0);
        verde_out       : out    vl_logic_vector(9 downto 0);
        azul_out        : out    vl_logic_vector(9 downto 0)
    );
end vga;
