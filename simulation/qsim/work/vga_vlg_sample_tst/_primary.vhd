library verilog;
use verilog.vl_types.all;
entity vga_vlg_sample_tst is
    port(
        azul_in         : in     vl_logic_vector(5 downto 0);
        clock           : in     vl_logic;
        reset           : in     vl_logic;
        rojo_in         : in     vl_logic_vector(5 downto 0);
        verde_in        : in     vl_logic_vector(5 downto 0);
        sampler_tx      : out    vl_logic
    );
end vga_vlg_sample_tst;
