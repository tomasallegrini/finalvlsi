library ieee;
library lpm;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use lpm.lpm_components.all;

entity vga is
	generic (constant divider : integer := 1023); -- 2^10 = 1024
	port(
		--entradas
		reset : in std_logic;
		clock : in std_logic; -- Clock en 50 MHz
		rojo_in, verde_in, azul_in : in std_logic_vector(5 downto 0); --entradas fondo
																						  
		--salidas
		vga_clk : out std_logic; -- Clock en 25 MHz en la salida
		vga_sync : out std_logic;
		vga_blank : out std_logic; 
		vga_vs : out std_logic;
		vga_hs : out std_logic;
		rojo_out, verde_out, azul_out : out std_logic_vector(9 downto 0) --salidas fondo	
		);
end vga;

architecture maquina of vga is
	--estados
	type state_type is (state_inicio, state_a, state_b, state_c, state_d);
	
	--señales
	signal state : state_type; --para la maquina de estados que genera las señales horizontales
	signal state2 : state_type; --para la maquina de estados que genera las señales verticales
	signal rgb_hab1 : std_logic; --h_blanck
	signal rgb_hab2 : std_logic; --V_blank
	signal horizontal : std_logic; --VGA_hs
	signal clock_reducido : std_logic; --Reloj a 25MHz

begin
	vga_clk <= clock_reducido;
	vga_sync <= '0';
	
	--Division frecuencia del reloj de 50MHz a 25MHz
	divisor_frecuencia: process(clock)
	variable contador : integer range 0 to 1; -- escala = 50/25 = 2
	begin
		if reset = '1' then 
			clock_reducido <= '0'; --reinicio reloj
			contador := 0; --reinicio contador
		elsif rising_edge(clock) then
			if contador = 1 then
				clock_reducido <= '1';
				contador := 0;
			else
				clock_reducido <= '0';
				contador := contador + 1;
			end if; 
		end if;
end process;

--Proceso para generar las señales VGA_HS y H_Blank(horizontal)
process(clock_reducido) --es sensible al clock de 25 MHz
variable contador : integer range 0 to 1023; -- el contador me hace evolucionar los pixeles 
	
	begin
		--Genero las señales con una maquina de estados
		if reset = '1' then 
			state <= state_inicio; --con el reset volvemos al estado inicial
			contador := 0;
			
			rgb_hab1 <= '0'; --h_blank
		elsif rising_edge(clock_reducido) then
			case state is
				when state_inicio =>
					state <= state_a;
					
				when state_a =>
					if contador = 4 then --95
						state <= state_b;
						contador := 0;
					else 
						contador := contador + 1;
					end if;
					
				when state_b =>
					if contador = 2 then --47 
						state <= state_c;
						contador := 0;
					else 
						contador := contador + 1;
					end if;
					
				when state_c =>
					rgb_hab1 <= '1'; --h_blank
					
					if contador = 20 then --639
						state <= state_d;
						contador := 0;
					else 
						contador := contador + 1;
					end if;
					
				when state_d =>
					rgb_hab1 <= '0'; --h_blank
					
					if contador = 2 then  --15
						state <= state_a;
						contador := 0;
					else 
						contador := contador +1;
					end if;
				end case;
		end if;
end process;

--señal vga_hs
vga_hs <= horizontal; 
with state select
horizontal <= '0' when state_a,
				  '1' when others;

--Proceso para generar la señal VGA_HS (vertical) (sensible a la senial horizontal)
process (horizontal)
	variable contador: integer range 0 to 1023;

	begin
		if reset = '1' then
			state2 <= state_inicio;
			contador := 0;
			
			rgb_hab2 <= '0';
		elsif falling_edge(horizontal) then
			case state2 is
				when state_inicio =>
					state2 <= state_a;

				when state_a =>
					if contador = 1 then --1
						state2 <= state_b;
						contador := 0;
					else
						contador := contador + 1;
					end if;

				when state_b =>
					if contador = 2 then --32
						state2 <= state_c;
						contador := 0;
					else
						contador := contador + 1;
					end if;
				
				when state_c =>
					rgb_hab2 <= '1'; --V_Blank
					
					if contador = 10 then --479
						state2 <= state_d;
						contador := 0;
					else
						contador := contador + 1;
					end if;

				when state_d =>
					rgb_hab2 <= '0';
					
					if contador = 2 then --9
						state2 <= state_inicio;
						contador := 0;
					else
						contador := contador + 1;
					end if;
			end case;
		end if;
	end process;

--señal vga_vs
with state2 select
vga_vs <= '0' when state_a,
	  '1' when others;

vga_blank <= rgb_hab1 and rgb_hab2; --Cuando ambas señales esten en uno, estara disponible la señal de los pixeles a color 


-- definicion de los colores
rojo_out(9) <= rojo_in(5);
rojo_out(8) <= rojo_in(4);
rojo_out(7) <= rojo_in(3);
rojo_out(6) <= rojo_in(3);
rojo_out(5) <= rojo_in(2);
rojo_out(4) <= rojo_in(2);
rojo_out(3) <= rojo_in(1);
rojo_out(2) <= rojo_in(1);
rojo_out(1) <= rojo_in(0);
rojo_out(0) <= rojo_in(0);

verde_out(9) <= verde_in(5);
verde_out(8) <= verde_in(4);
verde_out(7) <= verde_in(3);
verde_out(6) <= verde_in(3);
verde_out(5) <= verde_in(2);
verde_out(4) <= verde_in(2);
verde_out(3) <= verde_in(1);
verde_out(2) <= verde_in(1);
verde_out(1) <= verde_in(0);
verde_out(0) <= verde_in(0);

azul_out(9) <= azul_in(5);
azul_out(8) <= azul_in(4);
azul_out(7) <= azul_in(3);
azul_out(6) <= azul_in(3);
azul_out(5) <= azul_in(2);
azul_out(4) <= azul_in(2);
azul_out(3) <= azul_in(1);
azul_out(2) <= azul_in(1);
azul_out(1) <= azul_in(0);
azul_out(0) <= azul_in(0);

end maquina;
